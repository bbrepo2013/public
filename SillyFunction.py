class Product:
	"""
	The class for instantiating a product.
	Attrributes:
		brand,
		typ
	"""

	def __init__(self, brand, typ):
		self.brand = brand
		self.typ = typ

def SillyFunction(products):
	brand_type = {}
	for p in products:
		brand_type.setdefault(p.typ, []).append(p.brand)
	return brand_type

def testSillyFunction(products=[], answers={}):
	"""
	Test case for SillyFunction.
	A custom products list and answers may be parsed to the test case.
	In the absence of custom products list and answers, following hard coded
	values are used.
	"""
	if not products and not answers:
		# Test data
		products = [Product("aa", "A"), Product("ab", "A"), Product("bc", "B"), Product("cb", "C"), Product("ba", "B"), Product("ca", "C"), Product("ac", "A"), Product("bb", "B")]
		answers = {
			'A': ['aa', 'ab', 'ac'],
			'B': ['ba', 'bb', 'bc'],
			'C': ['ca', 'cb'],
		}

	brand_type = SillyFunction(products)
	try:
		for bt in sorted(brand_type.keys()):
			assert  sorted(brand_type[bt]) == answers[bt], "Categorization for brand type %s is not correct. Expected: %s. Returned: %s" % (bt, answers[bt], brand_type[bt])

	except AssertionError as ae:
		print(ae.message)


if __name__ == "__main__":
	print("Testing with correct answers:")
	# Testing with hard coded data.
	testSillyFunction()
	print("\tCompleted.\n")

	print("Testing with incorrect answers:")
	# In the following, the answer for brand type 'C' is not correct.
	products = [Product("aa", "A"), Product("ab", "A"), Product("bc", "B"), Product("cb", "C"), Product("ba", "B"), Product("ca", "C"), Product("ac", "A"), Product("bb", "B")]
	incorrect_answers = {
		'A': ['aa', 'ab', 'ac'],
		'B': ['ba', 'bb', 'bc'],
		'C': ['ca', 'cb', 'cc'],
	}
	# Testing with custom and incorrect data.
	testSillyFunction(products=products, answers=incorrect_answers)
	print("\tCompleted.\n")

	print("""
Hi, 

I'm Gaya Jayasinghe. I haven't coded with perl before. 
So I answered using python. I hope that I've got 
the logic correct for the perl code written on an April 
Fools day.

Thank you for reviwing my application. Look forward to here
from you.
"""
)
